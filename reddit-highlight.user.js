// ==UserScript==
// @name        reddit-highlight-2
// @namespace   reddit-highlight
// @include     /^https?://([a-z]+\.|)reddit\.com//
// @version     1
// @grant       GM_getValue
// @grant       GM_setValue
// @homepageURL https://github.com/opennota/reddit-highlight-2
// ==/UserScript==

/* global GM */

const reqTime = new Date();

const newCommentColor = '#ADFFB2';
const commentsLinkColor = '#FF4500'; // OrangeRed

function addStyle(css) {
  const style = document.createElement('style');
  style.textContent = css;
  document.head.appendChild(style);
}

async function getLastVisited(path) {
  const s = await GM_getValue(path);
  return s ? new Date(s) : undefined;
}

function setLastVisited(path, time) {
  GM_setValue(path, time.toISOString());
}

function observe(selector, callback) {
  const observer = new MutationObserver(mutations => {
    mutations.forEach(mutation => {
      Array.forEach(mutation.addedNodes, callback);
    });
  });
  const el = document.querySelector(selector);
  if (el) {
    observer.observe(el, {
      childList: true,
      subtree: true,
    });
  }
}

function getUser() {
  const userLink = document.querySelector('.user > a[href]');
  return userLink ? userLink.textContent : undefined;
}

function getCommentTime(comment) {
  let timeEl = comment.querySelector(
    ':scope > .entry > .tagline > time.edited-timestamp'
  );
  if (timeEl) {
    return new Date(timeEl.getAttribute('datetime'));
  }
  timeEl = comment.querySelector(
    ':scope > .entry > .tagline > time.live-timestamp'
  );
  return timeEl ? new Date(timeEl.getAttribute('datetime')) : undefined;
}

async function processComments() {
  const user = getUser();

  const highlight = comment => {
    const authorLink = comment.querySelector(
      ':scope > .entry > .tagline > a.author'
    );
    if (authorLink && authorLink.textContent === user) return;
    comment.querySelector('.usertext-body').className += ' highlight';
  };

  const path = location.pathname.replace(/^\/r\/|\/$/g, '');
  const lastVisited = await getLastVisited(path);

  const conditionalHighlight = comment => {
    if (getCommentTime(comment) > lastVisited) {
      highlight(comment);
    }
  };

  const highlightFunc = lastVisited ? conditionalHighlight : highlight;
  Array.forEach(document.querySelectorAll('.comment'), highlightFunc);
  setLastVisited(path, reqTime);

  observe('.commentarea', node => {
    if (node.nodeType !== Node.ELEMENT_NODE) return;
    if (node.classList.contains('comment')) {
      highlightFunc(node);
    }
  });

  addStyle(`.highlight { background-color: ${newCommentColor} !important; }`);

  const sitetable = document.querySelector('.commentarea > .sitetable');
  const hsb = document.createElement('div');
  hsb.className = 'highlight-since-box';
  hsb.innerHTML = `<span>Highlight comments since (hh:mm ago):</span>
    <input type="text" value="01:00">
    <button>highlight</button>`;
  sitetable.parentNode.insertBefore(hsb, sitetable);
  const input = hsb.querySelector('input');
  hsb.querySelector('button').addEventListener('click', () => {
    const [hours, minutes] = input.value.split(':');
    const diff = (parseInt(hours, 10) * 60 + parseInt(minutes, 10)) * 60 * 1000;
    const now = new Date();

    const highlightSince = comment => {
      const authorLink = comment.querySelector(
        ':scope > .entry > .tagline > a.author'
      );
      if (authorLink && authorLink.textContent === user) return;

      const body = comment.querySelector('.usertext-body');
      const highlighted = body.className.includes('highlight');
      if (now - getCommentTime(comment) <= diff) {
        if (!highlighted) {
          body.className += ' highlight';
        }
      } else if (highlighted) {
        body.className = body.className.replace('highlight', '');
      }
    };

    Array.forEach(document.querySelectorAll('.comment'), highlightSince);
  });

  addStyle(`
    .highlight-since-box {
      color: #583800;
      background-color: #FFFDCC;
      max-width: 500px;
      border: 1px solid #E1B000;
      font-weight: bold;
      padding: 7px 10px 7px 7px;
      display: inline-block;
      margin-left: 10px;
      font-size: 12px;
    }
    .highlight-since-box input {
      text-align: right;
      margin: auto 5px;
      width: 50px;
      height: 20px;
    }`);
}

function processLinks() {
  const re = new RegExp('^https?://([a-z]+\\.|)reddit\\.com/r/|/$', 'g');

  const conditionalColorLink = async link => {
    const path = link.href.replace(re, '');
    if (!await getLastVisited(path)) {
      link.className += ' unvisited';
    }
  };

  const colorLinks = root => {
    Array.forEach(root.querySelectorAll('a.comments'), conditionalColorLink);
  };

  colorLinks(document);

  observe('#siteTable', node => {
    if (node.nodeType !== Node.ELEMENT_NODE) return;
    if (node.tagName === 'A' && node.classList.contains('comments')) {
      conditionalColorLink(node);
    }
    colorLinks(node);
  });

  addStyle(`.unvisited { color: ${commentsLinkColor} !important; }`);
}

if (!/Ow! -- reddit.com/.test(document.querySelector('title').textContent)) {
  if (/^\/r\/[^/]+\/comments\//.test(location.pathname)) {
    processComments();
  } else {
    processLinks();
  }
}
// vim: ts=2 sts=2 sw=2 et
